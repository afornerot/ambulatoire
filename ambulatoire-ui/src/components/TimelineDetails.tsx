import { Card, Flex, HStack, VStack } from "@chakra-ui/react";
import { Advice, TimelineStep } from "../services/user-timeline-service";
import StepCard from "./StepCard";
import AdvicesCard from "./AdvicesCard";

interface Props {
  steps: TimelineStep[];
  selectedIndex: number;

  onExpand: (index: number) => void;
  onStepDateChanged: (newDate: Date | null, step: TimelineStep) => void;
  onAdviceDateChanged: (newDate: Date | null, advice: Advice) => void;
}

const TimelineDetails = ({
  steps,

  selectedIndex,
  onExpand,
  onStepDateChanged,
  onAdviceDateChanged,
}: Props) => {
  return (
    <VStack width="100%">
      {steps.map((step: TimelineStep, i) => (
        <StepCard
          key={i}
          step={step}
          onExpand={() => onExpand(i)}
          hideDetails={i < selectedIndex}
          onDateChange={(date: Date | null) => onStepDateChanged(date, step)}
          onAdviceDateChanged={onAdviceDateChanged}
        />
      ))}
    </VStack>
  );
};

export default TimelineDetails;
