import { Button, HStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import DatePicker from "./Generic/DatePicker";
import { CreateButton, CustomCloseButton } from "./Generic/CustomButtons";

interface Props {
  initialDate: Date | null;
  submitDate: (date: Date | null) => void;
}
const DateCard = ({ initialDate, submitDate }: Props) => {
  const [date, setDate] = useState(
    initialDate ? new Date(initialDate) : undefined
  );
  const [isAdding, setIsAdding] = useState(false);
  if (isAdding || date) {
    return (
      <HStack width="100%">
        <DatePicker
          date={date}
          setDate={(date: Date) => {
            setDate(date);
            submitDate(date);
          }}
          width="50%"
        />

        <CustomCloseButton
          size="sm"
          onClick={() => {
            setIsAdding(false);
            setDate(undefined);
            submitDate(null);
          }}
        />
      </HStack>
    );
  }
  return <Button onClick={() => setIsAdding(true)}>Planifier</Button>;
};

export default DateCard;
