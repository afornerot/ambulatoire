import { Table, TableContainer, Tbody } from "@chakra-ui/react";
import { Advice } from "../services/user-timeline-service";
import AdviceCard from "./AdviceCard";
import CategoryHeader from "./CategoryHeader";
interface Props {
  advices: Advice[];
  onAdviceDateChanged: (newDate: Date | null, advice: Advice) => void;
}

const AdvicesCard = ({ advices, onAdviceDateChanged }: Props) => {
  return (
    <TableContainer>
      <Table>
        <Tbody>
          {advices
            .sort((a, b) => a.category.id - b.category.id)
            .map((advice, i) => (
              <>
                {(i === 0 ||
                  advices[i - 1].category.id !== advice.category.id) && (
                  <CategoryHeader key={i + 1000} category={advice.category} />
                )}
                <AdviceCard
                  key={i}
                  advice={advice}
                  onDateChange={(date: Date | null) =>
                    onAdviceDateChanged(date, advice)
                  }
                />
              </>
            ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
};

export default AdvicesCard;
