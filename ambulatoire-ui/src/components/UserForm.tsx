import {
  Button,
  Center,
  Heading,
  Spinner,
  VStack,
  useToast,
} from "@chakra-ui/react";
import { useState } from "react";
import { submitForm } from "../actions/user-timeline-actions";
import { useNavigate } from "react-router-dom";
import { useToastError } from "../hooks/useCustomToast";
import { UserTimeline } from "../services/user-timeline-service";
import usePathologies, { Pathology } from "../hooks/usePathologies";
import { BodyComponent } from "reactjs-human-body";
import { PartsInput } from "reactjs-human-body/dist/components/BodyComponent/BodyComponent";
import {
  useLocalStorageRead,
  useLocalStorageStore,
} from "../hooks/useLocalStorage";
/*
type BodyPart =
  | "head"
  | "leftShoulder"
  | "rightShoulder"
  | "leftArm"
  | "rightArm"
  | "chest"
  | "stomach"
  | "leftLeg"
  | "rightLeg"
  | "rightHand"
  | "leftHand"
  | "leftFoot"
  | "rightFoot";
*/
const UserForm = () => {
  const { data: pathologies, isLoading } = usePathologies();

  const toast = useToast();
  const navigate = useNavigate();
  const onSubmit = (pathology: Pathology) => {
    const onSubmitSuccess = (userTimeline: UserTimeline) => {
      useLocalStorageStore("uuid", userTimeline.uuid);
      navigate(`/parcours/${userTimeline.uuid}`, {
        state: { userTimeline: userTimeline },
      });
      //useToastSuccess(toast, "Bienvenue!");
    };
    const onSubmitError = () => {
      useToastError(toast, "Error", "Une Erreur s'est produite");
    };
    const onSubmitFinally = () => {};
    submitForm(
      { pathologyid: pathology.id },
      onSubmitSuccess,
      onSubmitError,
      onSubmitFinally
    );
  };
  const [bodyParts, __] = useState({} as PartsInput);
  const [bodyModel, ___] = useState<"female" | "male">("male");

  const onClick = (id: string) => {
    if (!pathologies) {
      useToastError(toast, "Erreur", "Problème de connexion serveur");
      navigate("/");
      return;
    }

    const path = pathologies.find(
      (path: Pathology) => path.localisation === id
    );
    if (!path) return;
    onSubmit(path);
  };
  const storedUuid = useLocalStorageRead("uuid");

  if (isLoading) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }
  return (
    <VStack>
      <Heading>Selectionner votre pathologie</Heading>
      {storedUuid && (
        <Button
          mt="2em"
          colorScheme="teal"
          variant={"outline"}
          onClick={() => navigate(`/parcours/${storedUuid}`)}
        >
          Retour à mon parcours
        </Button>
      )}
      <BodyComponent
        partsInput={bodyParts}
        onClick={onClick}
        bodyModel={bodyModel}
      />
    </VStack>
  );
};

export default UserForm;
