import { Heading, Td, Tooltip, Tr } from "@chakra-ui/react";

import { Advice } from "../services/user-timeline-service";

import DateCard from "./DateCard";
interface Props {
  advice: Advice;
  onDateChange: (date: Date | null) => void;
}

const AdviceCard = ({ advice, onDateChange }: Props) => {
  return (
    <Tr lineHeight={"0.5em"}>
      <Td>
        {advice.isdate && (
          <DateCard initialDate={advice.date} submitDate={onDateChange} />
        )}
      </Td>

      <Td width="1%" justifyContent="left">
        <Tooltip
          label={
            <div dangerouslySetInnerHTML={{ __html: advice.description }} />
          }
        >
          <Heading size="sm" textColor={advice.category.color}>
            {advice.title}
          </Heading>
        </Tooltip>
      </Td>
    </Tr>
  );
};

export default AdviceCard;
