// @ts-nocheck
import React from "react";

import * as FaIcons from "react-icons/fa";
import * as FiIcons from "react-icons/fi";
import * as MdIcons from "react-icons/md";
import * as ImIcons from "react-icons/im";

interface Props {
  name: string;
}
export const CustomFaIcon = ({ name }: Props) => {
  const Icon = FaIcons[name];
  if (!Icon) return <p>Icon not found!</p>;

  return <Icon />;
};

export const CustomFiIcon = ({ name }: Props) => {
  const Icon = FiIcons[name];
  if (!Icon) return <p>Icon not found!</p>;

  return <Icon />;
};
export const CustomMdIcon = ({ name }: Props) => {
  const Icon = MdIcons[name];
  if (!Icon) return <p>Icon not found!</p>;

  return <Icon />;
};

export const CustomImIcon = ({ name }: Props) => {
  const Icon = ImIcons[name];
  if (!Icon) return <p>Icon not found!</p>;

  return <Icon />;
};
