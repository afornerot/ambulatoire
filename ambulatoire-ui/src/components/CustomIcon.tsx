import { BsCalendar } from "react-icons/bs";
import {
  CustomFaIcon,
  CustomFiIcon,
  CustomImIcon,
  CustomMdIcon,
} from "./Icons";
interface Props {
  name: string;
}
const CustomIcon = ({ name }: Props) => {
  switch (name.substring(0, 2).toLowerCase()) {
    case "fa":
      return <CustomFaIcon name={name} />;
    case "mi":
      return <CustomMdIcon name={name} />;
    case "im":
      return <CustomImIcon name={name} />;
    case "fi":
      return <CustomFiIcon name={name} />;
  }
  return <BsCalendar />;
};

export default CustomIcon;
