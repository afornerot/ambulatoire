import { extendTheme, ThemeConfig } from "@chakra-ui/react";
// @ts-nocheck - may need to be at the start of file

const config: ThemeConfig = {
  initialColorMode: "dark",
};
const theme = extendTheme({ config });
export default theme;
