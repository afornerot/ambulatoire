import {
  Box,
  Flex,
  Heading,
  Step,
  StepDescription,
  StepIcon,
  StepIndicator,
  StepNumber,
  StepSeparator,
  StepStatus,
  StepTitle,
  Stepper,
  useSteps,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { TimelineStep } from "../services/user-timeline-service";
import Tags from "./Tags";

interface Props {
  currentStepIndex: number;
  steps: TimelineStep[];
}
const TimelineStepper = ({ currentStepIndex, steps }: Props) => {
  const { activeStep, setActiveStep } = useSteps({
    index: 0,
    count: steps.length,
  });
  useEffect(() => setActiveStep(currentStepIndex), [currentStepIndex]);

  return (
    <Stepper
      index={activeStep}
      orientation="vertical"
      height="100vh"
      gap="0"
      position={"fixed"}
      top="5"
      left="20"
    >
      {steps.map((step, index) => (
        <Step key={index}>
          <StepIndicator>
            <StepStatus
              complete={<StepIcon />}
              incomplete={<StepNumber />}
              active={<StepNumber />}
            />
          </StepIndicator>

          <Box flexShrink="0">
            <StepTitle>
              <Heading size="md">{step.title}</Heading>
            </StepTitle>
            <StepDescription>
              <Flex
                flex=".5"
                gap="2"
                alignItems={"center"}
                mt="10"
                flexWrap={"wrap"}
              >
                <Tags tags={step.tags} />
              </Flex>
            </StepDescription>
          </Box>
          <StepSeparator />
        </Step>
      ))}
    </Stepper>
  );
};

export default TimelineStepper;
