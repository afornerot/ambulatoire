import { Badge } from "@chakra-ui/react";
import { colors } from "../context/colorContext";

interface Props {
  tags: string;
}
const Tags = ({ tags }: Props) => {
  if (!tags) return null;
  return tags.split(",").map((tag, i) => (
    <Badge key={i} fontSize="lg" colorScheme={colors[i % colors.length]}>
      {tag}
    </Badge>
  ));
};

export default Tags;
