import {
  Box,
  Card,
  CardHeader,
  Collapse,
  Divider,
  Flex,
  HStack,
  Heading,
} from "@chakra-ui/react";
import { useState } from "react";
import { Advice, TimelineStep } from "../services/user-timeline-service";
import AdviceCard from "./AdvicesCard";
import CustomIcon from "./CustomIcon";
import DateCard from "./DateCard";

interface Props {
  step: TimelineStep;
  onExpand: () => void;
  hideDetails: boolean;
  onDateChange: (newDate: Date | null) => void;
  onAdviceDateChanged: (newDate: Date | null, advice: Advice) => void;
}
const StepCard = ({
  step,
  onExpand,
  hideDetails,
  onDateChange,
  onAdviceDateChanged,
}: Props) => {
  return (
    <Card width={"100%"}>
      <CardHeader onClick={onExpand}>
        <Flex>
          <Flex flex="1" gap="4" alignItems={"center"} flexWrap={"wrap"}>
            <CustomIcon name={step.icon} />
            <HStack justifyContent={"space-evenly"}>
              <Heading>{step.title}</Heading>
              {step.isdate && (
                <DateCard submitDate={onDateChange} initialDate={step.date} />
              )}
            </HStack>

            <Divider />
          </Flex>
        </Flex>
      </CardHeader>
      <Collapse in={!hideDetails} animateOpacity>
        <AdviceCard
          onAdviceDateChanged={onAdviceDateChanged}
          advices={step.advices}
        />
      </Collapse>
    </Card>
  );
};

export default StepCard;
