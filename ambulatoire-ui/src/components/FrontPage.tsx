import {
  AspectRatio,
  Button,
  Card,
  CardBody,
  CardHeader,
  Heading,
  VStack,
} from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import ambulatoire from "../assets/ambulatoire.mp4";
const FrontPage = () => {
  const navigate = useNavigate();
  return (
    <>
      <Heading textAlign={"center"}>L'ambulatoire simplifié</Heading>
      <VStack height={"65vh"} mt="3em" spacing={"1em"}>
        <Button
          colorScheme="teal"
          variant={"outline"}
          onClick={() => navigate("/formulaire")}
        >
          Je suis concerné
        </Button>
        <Card>
          <CardBody>
            <VStack spacing="2em">
              <AspectRatio ratio={1} width={"800px"} height={"420px"}>
                <iframe title="ambulatoire" src={ambulatoire} allowFullScreen />
              </AspectRatio>
              <Heading textAlign="center" size="md">
                Vidéo explicative
              </Heading>
            </VStack>
          </CardBody>
        </Card>
      </VStack>
    </>
  );
};

export default FrontPage;
