import { Heading, Td, Tr } from "@chakra-ui/react";
import { Category } from "../services/user-timeline-service";
import CustomIcon from "./CustomIcon";
interface Props {
  category: Category;
}
const CategoryHeader = ({ category }: Props) => {
  return (
    <Tr>
      <Td width="1%" textColor={category.color}>
        <CustomIcon name={category.icon} />
      </Td>
      <Td>
        <Heading size="md" fontStyle={"italic"} textColor={category.color}>
          {category.title}
        </Heading>
      </Td>
    </Tr>
  );
};

export default CategoryHeader;
