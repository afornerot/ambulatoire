import { Button, useColorMode } from "@chakra-ui/react";
import { useContext } from "react";
import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import { ColorContext } from "../../context/colorContext";

const ColorModeSwitch = () => {
  const { toggleColorMode, colorMode } = useColorMode();
  const { color } = useContext(ColorContext);
  return (
    <Button onClick={toggleColorMode} size="md" colorScheme={color}>
      {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
    </Button>
  );
};

export default ColorModeSwitch;
