import {
  Box,
  Button,
  Center,
  HStack,
  Text,
  Spinner,
  VStack,
  useToast,
} from "@chakra-ui/react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import TimelineStepper from "./TimelineStepper";
import TimelineDetails from "./TimelineDetails";

import { useEffect, useState } from "react";
import {
  Advice,
  TimelineStep,
  UserTimeline,
} from "../services/user-timeline-service";
import {
  getTimeline,
  addAdviceDate,
  addStepDate,
} from "../actions/user-timeline-actions";
import { useToastError, useToastSuccess } from "../hooks/useCustomToast";
import { FaCalendarAlt } from "react-icons/fa";
import { icalUrl } from "../services/ical-service";
const Timeline = () => {
  const location = useLocation();

  const [timeline, setTimeline] =
    useState<UserTimeline>(/*{
    id: 1,
    uuid: "operation_genou",
    steps: [
      {
        id: 0,
        isdate: true,
        date: null,
        title: "Rendez-vous secrétaire",
        advices: [
          {
            date: new Date(),
            id: 3,
            title: "Lire les documents transmis",
            description: "Completer le passeport ambulatoire",
            isdate: false,
            category: {
              id: 1,
              title: "Pense-bête",
              description: "",
              color: "#123456",
              icon: "",
            },
          },
        ],
        description: "",
        tags: "",
        roworder: 0,
        chapter: "PRE",
        icon: "",
      },
      {
        id: 1,
        isdate: false,
        date: new Date(),
        title: "Intervention",
        advices: [
          {
            id: 4,
            date: null,
            title: "Ne pas manger",
            description: "Ne rien manger avant l opération",
            isdate: true,
            category: {
              title: "Pense-bête",
              description: "",
              color: "#123456",
              icon: "",
              id: 1,
            },
          },
        ],
        description: "",
        roworder: 0,
        tags: "jeun,test1,test2,test3",
        chapter: "PER",
        icon: "",
      },
    ],
    advices: [],
    pathology: {
      id: 1,
      description: "",
      localisation: "leftFoot",
      title: "Operation du pied",
    },
  }*/);

  const { uuid } = useParams();
  const toast = useToast();
  const navigate = useNavigate();

  const onStepDateChange = (newDate: Date | null, step: TimelineStep) => {
    if (!timeline) return;
    const onSuccess = (newTimeline: UserTimeline) => {
      setTimeline(newTimeline);
      useToastSuccess(toast, "Enregistré");
    };
    const onError = () => useToastError(toast, "Erreur");

    addStepDate(timeline.uuid, step.id, newDate, onSuccess, onError);
  };

  const onAdviceDateChange = (newDate: Date | null, advice: Advice) => {
    if (!timeline) return;
    const onSuccess = (newTimeline: UserTimeline) => {
      useToastSuccess(toast, "Enregistré");
      setTimeline(newTimeline);
    };
    const onError = () => useToastError(toast, "Erreur");

    addAdviceDate(timeline.uuid, advice.id, newDate, onSuccess, onError);
  };

  useEffect(() => {
    if (location.state?.userTimeline) {
      setTimeline(location.state.userTimeline);
    }
  }, [location.state]);

  useEffect(() => {
    if (!timeline) {
      if (uuid)
        getTimeline(
          uuid,
          (newTimeline: UserTimeline) => setTimeline(newTimeline),
          () => navigate("/")
        );
    }
  }, []);

  const [currentStep, setCurrentStep] = useState(0);
  if (!timeline) {
    return (
      <Center>
        <Spinner />
      </Center>
    );
  }
  return (
    <HStack justifyContent={"start"}>
      <Box width="30%">
        <TimelineStepper
          steps={timeline.steps}
          currentStepIndex={currentStep}
        />
      </Box>
      <VStack width="70%">
        <TimelineDetails
          steps={timeline.steps}
          selectedIndex={currentStep}
          onExpand={(i) => setCurrentStep(i)}
          onStepDateChanged={onStepDateChange}
          onAdviceDateChanged={onAdviceDateChange}
        />
      </VStack>
      <Box position={"fixed"} top="10" right="10">
        <Button colorScheme="green">
          <HStack>
            <FaCalendarAlt />
            <a href={icalUrl(timeline.uuid)}>Ajouter à votre calendrier</a>
          </HStack>
        </Button>
      </Box>
    </HStack>
  );
};

export default Timeline;
