import {
  AddIcon,
  CheckIcon,
  CloseIcon,
  DeleteIcon,
  DownloadIcon,
  EditIcon,
  HamburgerIcon,
  MinusIcon,
  SettingsIcon,
  ViewIcon,
} from "@chakra-ui/icons";
import { IconButton, IconButtonProps } from "@chakra-ui/react";
import { useState } from "react";
import {
  AiFillPauseCircle,
  AiFillPlayCircle,
  AiOutlineControl,
  AiOutlineFileSearch,
  AiOutlineUpload,
} from "react-icons/ai";
import { BiCopy, BiSortDown, BiSortUp } from "react-icons/bi";
import { FiMusic } from "react-icons/fi";
import { ImStatsDots } from "react-icons/im";
import useDefaultColor from "../../hooks/useDefaultColor";
import { BsFillBuildingFill, BsFillKeyFill } from "react-icons/bs";

const Button = (props: IconButtonProps) => {
  const [borderColor, setBorderColor] = useState<string | undefined>(undefined);
  const defaultColor = useDefaultColor();
  return (
    <IconButton
      {...props}
      borderRadius={"20px"}
      borderWidth={"2px"}
      borderColor={borderColor}
      onMouseEnter={() => setBorderColor(defaultColor)}
      onMouseLeave={() => setBorderColor(undefined)}
    />
  );
};

export const CreateButton = (props: IconButtonProps) => (
  <Button {...props} icon={<AddIcon />} />
);

export const MinusButton = (props: IconButtonProps) => (
  <Button {...props} icon={<MinusIcon />} />
);

export const CheckButton = (props: IconButtonProps) => (
  <Button {...props} aria-label="hello " icon={<CheckIcon />} />
);

export const CustomCloseButton = (props: IconButtonProps) => (
  <Button {...props} icon={<CloseIcon />} />
);

export const DownloadButton = (props: IconButtonProps) => (
  <Button {...props} icon={<DownloadIcon />} />
);

export const DeleteButton = (props: IconButtonProps) => (
  <Button {...props} icon={<DeleteIcon />} />
);
export const ViewButton = (props: IconButtonProps) => (
  <Button {...props} icon={<ViewIcon />} />
);

export const EditButton = (props: IconButtonProps) => (
  <Button {...props} icon={<EditIcon />} />
);

export const UploadButton = (props: IconButtonProps) => (
  <Button {...props} icon={<AiOutlineUpload />} />
);

export const SortDescendingButton = (props: IconButtonProps) => (
  <Button {...props} icon={<BiSortDown />} />
);
export const SortAscendingButton = (props: IconButtonProps) => (
  <Button {...props} icon={<BiSortUp />} />
);
export const InstitutionButton = (props: IconButtonProps) => (
  <Button {...props} icon={<BsFillBuildingFill />} />
);
export const KeyButton = (props: IconButtonProps) => (
  <Button {...props} icon={<BsFillKeyFill />} />
);

export const HamburgerButton = (props: IconButtonProps) => (
  <Button {...props} icon={<HamburgerIcon />} />
);
export const CopyButton = (props: IconButtonProps) => (
  <Button {...props} icon={<BiCopy />} />
);

export const LogsButton = (props: IconButtonProps) => (
  <Button {...props} icon={<AiOutlineFileSearch />} />
);
export const MusicButton = (props: IconButtonProps) => (
  <Button {...props} icon={<FiMusic />} />
);
export const PuiButton = (props: IconButtonProps) => (
  <Button {...props} icon={<AiOutlineControl />} />
);
export const StatisticsButton = (props: IconButtonProps) => (
  <Button {...props} icon={<ImStatsDots />} />
);
export const SettingsButton = (props: IconButtonProps) => (
  <Button {...props} icon={<SettingsIcon />} />
);
export const PlayButton = (props: IconButtonProps) => (
  <Button {...props} icon={<AiFillPlayCircle />} />
);
export const PauseButton = (props: IconButtonProps) => (
  <Button {...props} icon={<AiFillPauseCircle />} />
);
export const EmptyButton = (props: IconButtonProps) => <Button {...props} />;

CreateButton.defaultProps = {
  "aria-label": "create",
};

MinusButton.defaultProps = {
  "aria-label": "minus",
};
CheckButton.defaultProps = {
  "aria-label": "check",
};

CustomCloseButton.defaultProps = {
  "aria-label": "close",
};

DownloadButton.defaultProps = {
  "aria-label": "download",
};

DeleteButton.defaultProps = {
  "aria-label": "delete",
};

ViewButton.defaultProps = {
  "aria-label": "view",
};

EditButton.defaultProps = {
  "aria-label": "edit",
};

UploadButton.defaultProps = {
  "aria-label": "upload",
};

SortAscendingButton.defaultProps = {
  "aria-label": "sort",
};

SortDescendingButton.defaultProps = {
  "aria-label": "sort",
};

HamburgerButton.defaultProps = {
  "aria-label": "hamburger",
};

EmptyButton.defaultProps = {
  "aria-label": "empty",
};

CopyButton.defaultProps = {
  "aria-label": "copy",
};

LogsButton.defaultProps = {
  "aria-label": "go-to-logs",
};

MusicButton.defaultProps = {
  "aria-label": "go-to-music",
};

PuiButton.defaultProps = {
  "aria-label": "go-to-pui",
};

StatisticsButton.defaultProps = {
  "aria-label": "go-to-statistics",
};

SettingsButton.defaultProps = {
  "aria-label": "go-to-room-settings",
};

PlayButton.defaultProps = {
  "aria-label": "play",
};
PauseButton.defaultProps = {
  "aria-label": "pause",
};
InstitutionButton.defaultProps = {
  "aria-label": "institution",
};
KeyButton.defaultProps = {
  "aria-label": "key",
};
