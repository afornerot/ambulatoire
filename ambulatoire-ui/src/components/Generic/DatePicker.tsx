import { Box } from "@chakra-ui/react";
import { SingleDatepicker } from "chakra-dayzed-datepicker";

interface Props {
  date: Date | undefined;
  setDate: (date: Date) => void;
  width: string;
}
const DatePicker = ({ date, setDate, width }: Props) => {
  console.log(date);
  return (
    <Box width={width}>
      <SingleDatepicker name="date-input" date={date} onDateChange={setDate} />
    </Box>
  );
};

export default DatePicker;
