import { createContext } from "react";

interface ColorContextObject {
  color: string;
  setColor: React.Dispatch<React.SetStateAction<string>>;
}
export const ColorContext = createContext<ColorContextObject>(
  {} as ColorContextObject
);
export const colors = ["purple", "blue", "green", "red"];
export const hexColors = ["#ffcd56", "#36a2eb", "#4bc0c0", "#ff6384"];
export const gradient = ".400";
export const defaultColor = "teal";
