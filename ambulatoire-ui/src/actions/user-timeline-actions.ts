import routeAdviceService from "../services/route-advice-service";
import routeStepService from "../services/route-step-service";
import userTimelineService, {
  RouteCreateInterface,
} from "../services/user-timeline-service";
import { UserTimeline } from "../services/user-timeline-service";

export const submitForm = (
  form: RouteCreateInterface,
  onSuccess: (userTimeline: UserTimeline) => void,
  onError: () => void,
  onFinally: () => void
) => {
  userTimelineService
    .post(form)
    .then(({ data }) => onSuccess(data))
    .catch(onError)
    .finally(onFinally);
};

export const addStepDate = (
  routeuuid: string,
  stepid: number,
  date: Date | null,
  onSuccess: (newTimeline: UserTimeline) => void,
  onError: () => void
) => {
  if (date) date.setHours(12);
  routeStepService
    .post({
      routeuuid: routeuuid,
      stepid: stepid,
      stepdate: date ? date.toISOString().slice(0, 10) : "",
    })
    .then(({ data }) => onSuccess(data))
    .catch(onError);
};
export const getTimeline = (
  routeuuid: string,
  onSuccess: (newTimeline: UserTimeline) => void,
  onError: () => void
) =>
  userTimelineService
    .get(routeuuid)
    .then(({ data }) => onSuccess(data))
    .catch(onError);

export const addAdviceDate = (
  routeuuid: string,
  adviceid: number,
  date: Date | null,
  onSuccess: (newTimeline: UserTimeline) => void,
  onError: () => void
) => {
  if (date) date.setHours(12);
  routeAdviceService
    .post({
      routeuuid: routeuuid,
      adviceid: adviceid,
      advicedate: date ? date.toISOString().slice(0, 10) : "",
    })
    .then(({ data }) => onSuccess(data))
    .catch(onError);
};
