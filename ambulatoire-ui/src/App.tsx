import { Suspense } from "react";
import { Grid, GridItem } from "@chakra-ui/react";
import NavBar from "./components/Navigation/NavBar";
import FrontPage from "./components/FrontPage";
import { Route, Routes } from "react-router-dom";
import Timeline from "./components/Timeline";
import UserForm from "./components/UserForm";

function App() {
  return (
    <Suspense fallback={null}>
      <Grid
        templateAreas={{
          md: `"nav" "mainpanel"`,
          lg: `"nav" "mainpanel"`,
        }}
        templateColumns={{
          md: "1fr",
          lg: "1fr",
        }}
      >
        <GridItem area="nav">
          <NavBar />
        </GridItem>

        <GridItem area="mainpanel">
          <Routes>
            <Route path="/" element={<FrontPage />} />
            <Route path="/parcours" element={<Timeline />}>
              <Route path=":uuid"></Route>
            </Route>
            <Route path="/formulaire" element={<UserForm />} />
          </Routes>
        </GridItem>
      </Grid>
    </Suspense>
  );
}

export default App;
