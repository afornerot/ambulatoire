export function icalUrl(routeuuid: string) {
  return import.meta.env.VITE_ICAL_URL + "/" + routeuuid;
}
