import axios from "axios";
function apiKey() {
  return import.meta.env.VITE_API_KEY;
}
const headers = {
  accept: "*/*",
  key: apiKey(),
};

function apiDomain() {
  const { hostname } = window.location;
  let host = hostname;
  /*if (process.env.NODE_ENV !== "production") {
    //host = "192.168.1.17";
    //host = "192.168.1.252";
    //return "http://localhost:5000/api/v1";
  }*/
  //host = "192.168.1.245";
  return import.meta.env.VITE_API_URL;
  return (
    import.meta.env.VITE_API_PROTOCOL + host + import.meta.env.VITE_API_ENPOINT
  );
}

export default axios.create({
  baseURL: apiDomain(),
  headers: headers,
});

export const mediaApiClient = axios.create({
  baseURL: apiDomain(),
  responseType: "blob",
  headers: { "Content-Type": "multipart/form-data" },
});
