import apiClient, { mediaApiClient } from "./api-client";
import { AxiosRequestConfig } from "axios";

export interface Entity {
  id: number;
}

export interface DateQueryParameters {
  startDate: Date;
  endDate: Date;
}

export class HttpService {
  endpoint: string;

  constructor(endpoint: string) {
    this.endpoint = endpoint;
  }
  get(param: string, config?: AxiosRequestConfig) {
    return apiClient.get(this.endpoint + "/" + param, config);
  }
  getMedia(id: string, config: AxiosRequestConfig) {
    return mediaApiClient.get(this.endpoint, { ...config, params: { id: id } });
  }
  delete(id: string | number, config: AxiosRequestConfig) {
    return apiClient.delete(this.endpoint, { ...config, params: { id: id } });
  }

  post<T>(entity: T, config?: AxiosRequestConfig) {
    return apiClient.post(this.endpoint, {}, {
      ...config,
      headers: { ...config?.headers, ...entity },
    } as AxiosRequestConfig);
  }
  postMedia(data: FormData, config?: AxiosRequestConfig) {
    return mediaApiClient.post(this.endpoint, data, config);
  }
  update<T extends Entity>(id: number, entity: T, config: AxiosRequestConfig) {
    return apiClient.patch(this.endpoint, entity, {
      ...config,
      params: { id: id },
    });
  }
}

const create = (endpoint: string) => new HttpService(endpoint);
export default create;
