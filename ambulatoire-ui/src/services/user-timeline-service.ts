import { Pathology } from "../hooks/usePathologies";
import create, { Entity } from "./http-service";

export interface Advice extends Entity {
  title: string;
  description: string;
  isdate: boolean;
  date: Date | null;
  category: Category;
}
export interface Category extends Entity {
  title: string;
  description: string;
  color: string;
  icon: string;
}

export interface TimelineStep extends Entity {
  isdate: boolean;
  date: Date | null;
  title: string;
  advices: Advice[];
  description: string;
  roworder: number;
  chapter: string;
  icon: string;
  tags: string;
}

export interface UserTimeline extends Entity {
  uuid: string;
  steps: TimelineStep[];
  advices: Advice[];
  pathology: Pathology;
}
export interface RouteCreateInterface {
  pathologyid: number;
}
export interface StepDateChangeInterface {
  stepid: number;
  routeuuid: string;
  date: Date;
}
export interface CalendarCreateInterface {
  adviceid: number;
  routeuuid: string;
  date: Date;
}
export interface RouteCreateInterface {
  pathologyid: number;
}

export default create("/route");
