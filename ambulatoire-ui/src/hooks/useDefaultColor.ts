import { useColorMode } from "@chakra-ui/react";

const useDefaultColor = () => {
  const { colorMode } = useColorMode();
  return colorMode === "light" ? "black" : "white";
};
export default useDefaultColor;
