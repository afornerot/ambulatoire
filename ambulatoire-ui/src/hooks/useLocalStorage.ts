export const useLocalStorageStore = (key: string, data: string) =>
  localStorage.setItem(key, data);

export const useLocalStorageRead = (key: string) => localStorage.getItem(key);
