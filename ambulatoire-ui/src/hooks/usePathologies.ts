import useData from "./useData";

export interface Pathology {
  id: number;
  description: string | null;
  localisation: string;
  title: string;
}
const usePathologies = () => useData<Pathology[]>("/pathologies");

export default usePathologies;
