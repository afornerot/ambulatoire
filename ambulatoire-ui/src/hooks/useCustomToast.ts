import {
  AlertStatus,
  CreateToastFnReturn,
  ToastPosition,
} from "@chakra-ui/react";

const useToastParams = (
  title: string,
  status: AlertStatus,
  duration: number,
  position: ToastPosition,
  description?: string
) => {
  return {
    title: title,
    status: status,
    duration: duration,
    isClosable: true,
    position: position,
    description: description,
  };
};
export const useToastSuccess = (
  toast: CreateToastFnReturn,
  title: string,
  message?: string | null,
  duration = 3000
) => toast(useToastParams(title, "success", duration, "top", message || ""));

export const useToastError = (
  toast: CreateToastFnReturn,
  title: string,
  message?: string | null,
  duration = 3000
) => toast(useToastParams(title, "error", duration, "top", message || ""));

export const useToastWarning = (
  toast: CreateToastFnReturn,
  title: string,
  message?: string | null,
  duration = 3000
) => toast(useToastParams(title, "warning", duration, "top", message || ""));

export const useToastLoading = (
  toast: CreateToastFnReturn,
  title: string,
  message?: string | null,
  duration = 3000
) => toast(useToastParams(title, "loading", duration, "top", message || ""));
