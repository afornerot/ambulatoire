<?php

namespace App\Entity;

use App\Repository\RouteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RouteRepository::class)]
class Route
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::GUID)]
    private ?string $uuid = null;

    #[ORM\OneToMany(mappedBy: 'route', targetEntity: Routestep::class, orphanRemoval: true)]
    private Collection $routesteps;

    #[ORM\OneToMany(mappedBy: 'route', targetEntity: Routeadvice::class, orphanRemoval: true)]
    private Collection $routeadvices;

    #[ORM\ManyToOne(inversedBy: 'routes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Pathology $pathology = null;

    public function __construct()
    {
        $this->routesteps = new ArrayCollection();
        $this->routeadvices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): static
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return Collection<int, Routestep>
     */
    public function getRoutesteps(): Collection
    {
        return $this->routesteps;
    }

    public function addRoutestep(Routestep $routestep): static
    {
        if (!$this->routesteps->contains($routestep)) {
            $this->routesteps->add($routestep);
            $routestep->setRoute($this);
        }

        return $this;
    }

    public function removeRoutestep(Routestep $routestep): static
    {
        if ($this->routesteps->removeElement($routestep)) {
            // set the owning side to null (unless already changed)
            if ($routestep->getRoute() === $this) {
                $routestep->setRoute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Routeadvice>
     */
    public function getRouteadvices(): Collection
    {
        return $this->routeadvices;
    }

    public function addRouteadvice(Routeadvice $routeadvice): static
    {
        if (!$this->routeadvices->contains($routeadvice)) {
            $this->routeadvices->add($routeadvice);
            $routeadvice->setRoute($this);
        }

        return $this;
    }

    public function removeRouteadvice(Routeadvice $routeadvice): static
    {
        if ($this->routeadvices->removeElement($routeadvice)) {
            // set the owning side to null (unless already changed)
            if ($routeadvice->getRoute() === $this) {
                $routeadvice->setRoute(null);
            }
        }

        return $this;
    }

    public function getPathology(): ?Pathology
    {
        return $this->pathology;
    }

    public function setPathology(?Pathology $pathology): static
    {
        $this->pathology = $pathology;

        return $this;
    }
}
