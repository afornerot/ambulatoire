<?php

namespace App\Entity;

use App\Repository\AdviceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdviceRepository::class)]
class Advice
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToMany(targetEntity: Pathology::class, inversedBy: 'advices')]
    private Collection $pathologys;

    #[ORM\ManyToOne(inversedBy: 'advice')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Category $category = null;

    #[ORM\Column]
    private ?bool $date = null;

    #[ORM\OneToMany(mappedBy: 'advice', targetEntity: Routeadvice::class, orphanRemoval: true)]
    private Collection $routeadvices;

    #[ORM\ManyToOne(inversedBy: 'advices')]
    private ?Step $step = null;

    public function __construct()
    {
        $this->pathologys = new ArrayCollection();
        $this->routeadvices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Pathology>
     */
    public function getPathologys(): Collection
    {
        return $this->pathologys;
    }

    public function addPathology(Pathology $pathology): static
    {
        if (!$this->pathologys->contains($pathology)) {
            $this->pathologys->add($pathology);
        }

        return $this;
    }

    public function removePathology(Pathology $pathology): static
    {
        $this->pathologys->removeElement($pathology);

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function isDate(): ?bool
    {
        return $this->date;
    }

    public function setDate(bool $date): static
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Routeadvice>
     */
    public function getRouteadvices(): Collection
    {
        return $this->routeadvices;
    }

    public function addRouteadvice(Routeadvice $routeadvice): static
    {
        if (!$this->routeadvices->contains($routeadvice)) {
            $this->routeadvices->add($routeadvice);
            $routeadvice->setAdvice($this);
        }

        return $this;
    }

    public function removeRouteadvice(Routeadvice $routeadvice): static
    {
        if ($this->routeadvices->removeElement($routeadvice)) {
            // set the owning side to null (unless already changed)
            if ($routeadvice->getAdvice() === $this) {
                $routeadvice->setAdvice(null);
            }
        }

        return $this;
    }

    public function getStep(): ?Step
    {
        return $this->step;
    }

    public function setStep(?Step $step): static
    {
        $this->step = $step;

        return $this;
    }

}
