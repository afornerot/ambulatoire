<?php

namespace App\Entity;

use App\Repository\StepRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StepRepository::class)]
class Step
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column]
    private ?int $roworder = null;

    #[ORM\Column]
    private ?bool $date = null;

    #[ORM\OneToMany(mappedBy: 'step', targetEntity: Routestep::class, orphanRemoval: true)]
    private Collection $routesteps;

    #[ORM\Column(length: 255)]
    private ?string $icon = null;

    #[ORM\Column(length: 255)]
    private ?string $chapter = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tags = null;

    #[ORM\OneToMany(mappedBy: 'step', targetEntity: Advice::class)]
    private Collection $advices;

    public function __construct()
    {
        $this->routesteps = new ArrayCollection();
        $this->advices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getRoworder(): ?int
    {
        return $this->roworder;
    }

    public function setRoworder(int $roworder): static
    {
        $this->roworder = $roworder;

        return $this;
    }

    public function isDate(): ?bool
    {
        return $this->date;
    }

    public function setDate(bool $date): static
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Routestep>
     */
    public function getRoutesteps(): Collection
    {
        return $this->routesteps;
    }

    public function addRoutestep(Routestep $routestep): static
    {
        if (!$this->routesteps->contains($routestep)) {
            $this->routesteps->add($routestep);
            $routestep->setStep($this);
        }

        return $this;
    }

    public function removeRoutestep(Routestep $routestep): static
    {
        if ($this->routesteps->removeElement($routestep)) {
            // set the owning side to null (unless already changed)
            if ($routestep->getStep() === $this) {
                $routestep->setStep(null);
            }
        }

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): static
    {
        $this->icon = $icon;

        return $this;
    }

    public function getChapter(): ?string
    {
        return $this->chapter;
    }

    public function setChapter(string $chapter): static
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): static
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return Collection<int, Advice>
     */
    public function getAdvices(): Collection
    {
        return $this->advices;
    }

    public function addAdvice(Advice $advice): static
    {
        if (!$this->advices->contains($advice)) {
            $this->advices->add($advice);
            $advice->setStep($this);
        }

        return $this;
    }

    public function removeAdvice(Advice $advice): static
    {
        if ($this->advices->removeElement($advice)) {
            // set the owning side to null (unless already changed)
            if ($advice->getStep() === $this) {
                $advice->setStep(null);
            }
        }

        return $this;
    }
}
