<?php

namespace App\Form;

use App\Entity\Advice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AdviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('submit',
                SubmitType::class, [
                    'label' => 'Valider',
                    'attr' => ['class' => 'btn btn-success btn-lg'],
                ]
            )

            ->add('title',     
                TextType::class,[
                    'label' => 'Titre'
                ]
            )

            ->add('category', 
                EntityType::class, [
                    "label" => 'Categorie',
                    "class" => 'App\Entity\Category',
                    "choice_label" => 'title',
                    "placeholder" => "Selectionner une Catégorie",
                ]
            )   

            ->add('step', 
                EntityType::class, [
                    "label" => 'Etape',
                    "class" => 'App\Entity\Step',
                    "choice_label" => 'title',
                    "required" => false,
                    "placeholder" => "Selectionner une étape",
                ]
            ) 

            ->add('date', 
                CheckboxType::class, [
                    'label' => "Conseil pouvant être ajouté à l'agenda",
                    'required' => false,
                ]
            )

            ->add('pathologys', 
                Select2EntityType::class,[
                    'label' => 'Pathologies',
                    'required' => false,
                    'disabled' => false,
                    'multiple' => true,
                    "remote_route" => "app_pathology_select",
                    "class" => "App\Entity\Pathology",
                    "primary_key" => "id",
                    "text_property" => "title",
                    "minimum_input_length" => 0,
                    "page_limit" => 100,
                    "allow_clear" => true,
                    "delay" => 250,
                    "cache" => false,
                    "cache_timeout" => 60000,
                    "language" => "fr",
                    "placeholder" => "Selectionner des Pathologies",                    
                ]
            )

            ->add('description',
                CKEditorType::class,[
                    'label' => 'Description',
                    'required' => false,
                    'config' => ["height" => "500px"],
                ]
            )

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Advice::class,
            'mode' => "submit",
        ]);
    }
}
