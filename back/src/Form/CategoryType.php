<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('submit',
                SubmitType::class, [
                    'label' => 'Valider',
                    'attr' => ['class' => 'btn btn-success btn-lg'],
                ]
            )

            ->add('title',     
                TextType::class,[
                    'label' => 'Titre'
                ]
            )
            
            ->add('description',
                CKEditorType::class,[
                    'label' => 'Description',
                    'required' => false,
                    'config' => ["height" => "500px"],
                ]
            )

            ->add('color',
                TextType::class,[
                    'label' => 'Couleur',
                    'required' => true,
                    'attr' => ['class' => 'spectrum']
                ]
            )            
        
            ->add('icon',
                ChoiceType::class,[
                    'label' => 'Couleur',
                    'required' => true,
                    'choices' => [
                        'FaHouseUser' => 'FaHouseUser',
                        'FaInfoCircle' => 'FaInfoCircle',
                        'FaRegCalendarAlt' => 'FaRegCalendarAlt',
                        'FaSplotch' => 'FaSplotch',
                        'FaPhoneAlt' => 'FaPhoneAlt',
                        'FaDoorOpen' => 'FaDoorOpen',
                        'FaUserMd' => 'FaUserMd',
                        'FaBed' => 'FaBed',
                        'FaChild' => 'FaChild',
                        'FaClock' => 'FaClock',
                    ]
                ]
            )         
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
            'mode' => "submit",
        ]);
    }
}
