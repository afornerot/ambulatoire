<?php

namespace App\Form;

use App\Entity\Step;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

class StepType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('submit',
                SubmitType::class, [
                    'label' => 'Valider',
                    'attr' => ['class' => 'btn btn-success btn-lg'],
                ]
            )

            ->add('title',
                TextType::class,[
                    'label' => 'Titre'
                ]
            )

            ->add('tags',
                TextType::class,[
                    'label' => 'Tags (spérateur virgule)',
                    'required' => false,
                ]
            )
            
            ->add('chapter',
                ChoiceType::class,[
                    'label' => 'Chapitre',
                    'required' => true,
                    'choices' => [
                        'PRÉ' => 'PRÉ',
                        'PER' => 'PER',
                        'POST' => 'POST',
                    ]
                ]
            )

            ->add('description',
                CKEditorType::class,[
                    'label' => 'Description',
                    'required' => false,
                    'config' => ["height" => "500px"],
                ]
            )

            ->add('date', 
                CheckboxType::class, [
                    'label' => "Conseil pouvant être ajouté à l'agenda",
                    'required' => false,
                ]
            )

            ->add('icon',
                ChoiceType::class,[
                    'label' => 'Couleur',
                    'required' => true,
                    'choices' => [
                        'FaHouseUser' => 'FaHouseUser',
                        'FaInfoCircle' => 'FaInfoCircle',
                        'FaRegCalendarAlt' => 'FaRegCalendarAlt',
                        'FaSplotch' => 'FaSplotch',
                        'FaPhoneAlt' => 'FaPhoneAlt',
                        'FaDoorOpen' => 'FaDoorOpen',
                        'FaUserMd' => 'FaUserMd',
                        'FaBed' => 'FaBed',
                        'FaChild' => 'FaChild',
                        'FaClock' => 'FaClock',
                    ]
                ]
            )         
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Step::class,
            'mode' => "submit",
        ]);
    }
}
