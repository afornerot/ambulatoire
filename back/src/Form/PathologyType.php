<?php

namespace App\Form;

use App\Entity\Pathology;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PathologyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('submit',
                SubmitType::class, [
                    'label' => 'Valider',
                    'attr' => ['class' => 'btn btn-success btn-lg'],
                ]
            )

            ->add('title',     
                TextType::class,[
                    'label' => 'Titre'
                ]
            )
            
            ->add('localisation',
                ChoiceType::class,[
                    'label' => 'Localisation',
                    'choices' => [
                        'head' => 'head',
                        'leftShoulder' => 'leftShoulder',
                        'rightShoulder' => 'rightShoulder',
                        'leftArm' => 'leftArm',
                        'rightArm' => 'rightArm',
                        'chest' => 'chest',
                        'stomach' => 'stomach',
                        'leftLeg' => 'leftLeg',
                        'rightLeg' => 'rightLeg',
                        'leftHand' => 'leftHand',
                        'rightHand' => 'rightHand',
                        'leftFoot' => 'leftFoot',
                        'rightFoot' => 'rightFoot'                        
                    ]
                ])

            ->add('description',
                CKEditorType::class,[
                    'label' => 'Description',
                    'required' => false,
                    'config' => ["height" => "500px"],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Pathology::class,
            'mode' => "submit",
        ]);
    }
}
