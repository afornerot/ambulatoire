<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;

#[AsCommand(
    name: 'app:init',
    description: 'Add a short description for your command',
)]
class InitCommand extends Command
{
    private $em;
    private $passwordhasher;

    public function __construct(EntityManagerInterface $em,UserPasswordHasherInterface $passwordhasher)
    {
        parent::__construct();
        $this->em = $em;
        $this->passwordhasher = $passwordhasher;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // On s'assure que le user admin existe
        $io->success('Admin Submit');
        $user = $this->em->getRepository('App\Entity\User')->findOneBy(['username' => 'admin']);
        if (!$user) {
            $user = new User();
            $user->setUsername('admin');
            $user->setRoles(["ROLE_ADMIN"]);
            $hashed = $this->passwordhasher->hashPassword($user,"admin");            
            $user->setPassword($hashed);    
            $this->em->persist($user);
            $this->em->flush();
        }
        else {
            $hashed = $this->passwordhasher->hashPassword($user,"admin");            
            $user->setPassword($hashed);    
            $this->em->flush();
        }

                

        return Command::SUCCESS;
    }
}
