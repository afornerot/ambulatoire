<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Category;
use App\Form\CategoryType;

class CategoryController extends AbstractController
{
    #[Route('/category', name: 'app_category')]
    public function list(ManagerRegistry $em): Response
    {
        $categorys = $em->getRepository("App\Entity\Category")->findAll();

        return $this->render('category/list.html.twig', [
            'usesidebar' => true,
            'categorys' => $categorys,
        ]);
    }

    #[Route('/category/submit', name: 'app_category_submit')]
    public function submit(ManagerRegistry $em,Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category, ['mode' => 'submit']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $data = $form->getData();
            $em->getManager()->persist($data);
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_category'));
        }

        return $this->render('category/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "submit",
            'form' => $form->createView(),
            'category' => $category,
        ]);
    }  
    
    #[Route('/category/update/{id}', name: 'app_category_update')]
    public function update($id,ManagerRegistry $em,Request $request): Response
    {
        $category = $em->getRepository("App\Entity\Category")->find($id);
        $form = $this->createForm(CategoryType::class, $category, ['mode' => 'update']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_category'));
        }

        return $this->render('category/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "update",
            'form' => $form->createView(),
            'category' => $category,
        ]);
    } 
    
    #[Route('/category/delete/{id}', name: 'app_category_delete')]
    public function delete($id,ManagerRegistry $em,Request $request): Response
    {
        $category = $em->getRepository("App\Entity\Category")->find($id);
        
        try {
            $em->getManager()->remove($category);
            $em->getManager()->flush();
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            return $this->redirectToRoute('app_category_update', ['id' => $id]);
        }

        return $this->redirectToRoute('app_category');
    }     

    #[Route('/category/select', name: 'app_category_select')]
    public function select(ManagerRegistry $em,Request $request): Response
    {

        $output=array();
        $page_limit=$request->query->get('page_limit');
        $q=$request->query->get('q');
        
        $qb = $em->getManager()->createQueryBuilder();
        $qb->select('table')->from('App\Entity\Category','table')
           ->where('table.title LIKE :value')
           ->setParameter("value", "%".$q."%")
           ->orderBy('table.title');
        
        $datas=$qb->setFirstResult(0)->setMaxResults($page_limit)->getQuery()->getResult();
        foreach($datas as $data) {
            array_push($output,array("id"=>$data->getId(),"text"=>$data->getTitle()));
        }

        $ret_string["results"]=$output;
        return new JsonResponse($ret_string);    
    } 
}
