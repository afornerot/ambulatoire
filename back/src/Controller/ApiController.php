<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;
use Ramsey\Uuid\Uuid;

use App\Entity\Route;
use App\Entity\Routestep;
use App\Entity\Routeadvice;

class ApiController extends AbstractFOSRestController
{
    private $output = [];
    private $cpt;

    /**
     * pathologies.
     *
     * @FOSRest\Get("/api/v1/pathologies")
     * @OA\Response(
     *     response=200,
     *     description="get pathologies"
     *     )
     * )
     * @OA\Parameter(
     *     name="key",
     *     in="header",
     *     required=true,
     *     description="APIKey",
     *     @OA\Schema(type="string")
     * )
     */
    public function getPathogies(Request $request, ManagerRegistry $em)
    {
        // Récupération des parametres
        if (!$this->iskey($request->headers->get('key'))) {
            $view = $this->view('API Key inconnue', 403);
            return $this->handleView($view);
        }

        $output = [];
        $pathologys = $em->getRepository("App\Entity\Pathology")->findAll();
        foreach ($pathologys as $pathology) {
            array_push($output, [
                "id" => $pathology->getId(),
                "title" => $pathology->getTitle(),
                "localisation" => $pathology->getLocalisation(),
                "description" => $pathology->getDescription(),
            ]);
        }

        $view = $this->view($output, 200);
        return $this->handleView($view);
    }

    /**
     * route.
     *
     * @FOSRest\Post("/api/v1/route")
     * @OA\Response(
     *     response=200,
     *     description="submit route"
     *     )
     * )
     * @OA\Parameter(
     *     name="key",
     *     in="header",
     *     required=true,
     *     description="APIKey",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="pathologyid",
     *     in="header",
     *     required=true,
     *     description="ID de la Pathologie",
     *     @OA\Schema(type="integer")
     * )     
     */
    public function postRoute(Request $request, ManagerRegistry $em)
    {
        // Récupération des parametres
        if (!$this->iskey($request->headers->get('key'))) {
            $view = $this->view('API Key inconnue', 403);
            return $this->handleView($view);
        }

        $pathology=$em->getRepository("App\Entity\Pathology")->find($request->headers->get('pathologyid'));
        if(!$pathology) {
            $view = $this->view('Pathologie inconnue', 403);
            return $this->handleView($view);
        }

        $route = new Route();
        $route->setUuid(Uuid::uuid4());
        $route->SetPathology($pathology);
        
        $em->getManager()->persist($route);
        $em->getManager()->flush();  

        $output=$this->frmRoute($em,$route);
        $view = $this->view($output, 200);
        return $this->handleView($view);
    }

    /**
     * route.
     *
     * @FOSRest\Get("/api/v1/route/{uuid}")
     * @OA\Response(
     *     response=200,
     *     description="submit route"
     *     )
     * )
     * @OA\Parameter(
     *     name="key",
     *     in="header",
     *     required=true,
     *     description="APIKey",
     *     @OA\Schema(type="string")
     * )
     */
    public function getRoute($uuid,Request $request, ManagerRegistry $em)
    {
        // Récupération des parametres
        if (!$this->iskey($request->headers->get('key'))) {
            $view = $this->view('API Key inconnue', 403);
            return $this->handleView($view);
        }

        $route = $em->getRepository("App\Entity\Route")->findOneBy(["uuid"=>$uuid]);
        if(!$route) {
            $view = $this->view('Parcours inconnu', 403);
            return $this->handleView($view);
        }

        $output=$this->frmRoute($em,$route);
        $view = $this->view($output, 200);
        return $this->handleView($view);
    }

    /**
     * routestep
     *
     * @FOSRest\Post("/api/v1/routestep")
     * @OA\Response(
     *     response=200,
     *     description="submit routestep"
     *     )
     * )
     * @OA\Parameter(
     *     name="key",
     *     in="header",
     *     required=true,
     *     description="APIKey",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="routeuuid",
     *     in="header",
     *     required=true,
     *     description="UUID du parcours",
     *     @OA\Schema(type="string")
     * )     
     * @OA\Parameter(
     *     name="stepid",
     *     in="header",
     *     required=true,
     *     description="ID de l'étape",
     *     @OA\Schema(type="integer")
     * )  
     * @OA\Parameter(
     *     name="stepdate",
     *     in="header",
     *     required=false,
     *     description="Date de l'étape au format YYYY-MM-DD",
     *     @OA\Schema(type="string")
     * )  
     */
    public function postRoutestep(Request $request, ManagerRegistry $em)
    {
        // Récupération des parametres
        if (!$this->iskey($request->headers->get('key'))) {
            $view = $this->view('API Key inconnue', 403);
            return $this->handleView($view);
        }

        $route=$em->getRepository("App\Entity\Route")->findOneBy(["uuid"=>$request->headers->get('routeuuid')]);
        if(!$route) {
            $view = $this->view('Parcours inconnu', 403);
            return $this->handleView($view);
        }

        $step=$em->getRepository("App\Entity\Step")->find($request->headers->get('stepid'));
        if(!$step) {
            $view = $this->view('Etape inconnue', 403);
            return $this->handleView($view);
        }

        $routestep=$em->getRepository("App\Entity\Routestep")->findOneBy(["route"=>$route,"step"=>$step]);
        if(!$routestep) {
            $routestep = new Routestep();
            $routestep->setRoute($route);
            $routestep->SetStep($step);
            $em->getManager()->persist($routestep);
            $em->getManager()->flush(); 
        }

        $mydate=null;
        if($request->headers->get('stepdate')!="")
            $mydate=\DateTime::createFromFormat('Y-m-d',$request->headers->get('stepdate'));
        $routestep->setDate($mydate);
        $em->getManager()->flush();

        $output=$this->frmRoute($em,$route);
        $view = $this->view($output, 200);
        return $this->handleView($view);
    }
    /**
     * routeadvice
     *
     * @FOSRest\Post("/api/v1/routeadvice")
     * @OA\Response(
     *     response=200,
     *     description="submit routeadvice"
     *     )
     * )
     * @OA\Parameter(
     *     name="key",
     *     in="header",
     *     required=true,
     *     description="APIKey",
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="routeuuid",
     *     in="header",
     *     required=true,
     *     description="UUID du parcours",
     *     @OA\Schema(type="string")
     * )     
     * @OA\Parameter(
     *     name="adviceid",
     *     in="header",
     *     required=true,
     *     description="ID du conseil",
     *     @OA\Schema(type="integer")
     * )  
     * @OA\Parameter(
     *     name="advicedate",
     *     in="header",
     *     required=false,
     *     description="Date de l'étape au format YYYY-MM-DD",
     *     @OA\Schema(type="string")
     * )  
     */
    public function postRouteadvice(Request $request, ManagerRegistry $em)
    {
        // Récupération des parametres
        if (!$this->iskey($request->headers->get('key'))) {
            $view = $this->view('API Key inconnue', 403);
            return $this->handleView($view);
        }

        $route=$em->getRepository("App\Entity\Route")->findOneBy(["uuid"=>$request->headers->get('routeuuid')]);
        if(!$route) {
            $view = $this->view('Parcours inconnu', 403);
            return $this->handleView($view);
        }

        $advice=$em->getRepository("App\Entity\Advice")->find($request->headers->get('adviceid'));
        if(!$advice) {
            $view = $this->view('Conseil inconnue', 403);
            return $this->handleView($view);
        }

        $routeadvice=$em->getRepository("App\Entity\Routeadvice")->findOneBy(["route"=>$route,"advice"=>$advice]);
        if(!$routeadvice) {
            $routeadvice = new Routeadvice();
            $routeadvice->setRoute($route);
            $routeadvice->SetAdvice($advice);
            $em->getManager()->persist($routeadvice);
            $em->getManager()->flush(); 
        }

        $mydate=null;
        if($request->headers->get('advicedate')!="")
            $mydate=\DateTime::createFromFormat('Y-m-d',$request->headers->get('advicedate'));
    
        $routeadvice->setDate($mydate);
        $em->getManager()->flush();

        $output=$this->frmRoute($em,$route);
        $view = $this->view($output, 200);
        return $this->handleView($view);
    }










    private function frmRoute($em,$route) {
        $frmpathology=$this->frmPathology($em,$route->getPathology());
     
        $advices=$em->getRepository("App\Entity\Advice")->findBy(["step"=>null]);
        $frmadvices=[];
        foreach($advices as $advice) {
            $pathologys=$advice->getPathologys();
            $tosend=true;
            if(!$pathologys->isEmpty()) {
                $tosend=false;
                foreach($pathologys as $pathology) {
                    if($pathology==$route->getPathology()) $tosend=true;
                }
            }

            if($tosend) array_push($frmadvices,$this->frmAdvice($em,$route,$advice));
        }

        $steps=$em->getRepository("App\Entity\Step")->findBy([],["roworder"=>"ASC"]);
        $frmsteps=[];
        foreach($steps as $step) {
            array_push($frmsteps,$this->frmStep($em,$route,$step));
        }

        $output = [
            "id" => $route->getId(),
            "uuid" => $route->getUuid(),
            "pathology" => $frmpathology,
            "advices" => $frmadvices,
            "steps" => $frmsteps,
        ];
        
        return $output;
    }

    private function frmPathology($em,$pathology) {
        return [
            "id" => $pathology->getId(),
            "title" => $pathology->getTitle(),
            "description" => $pathology->getDescription(),
            "localisation" => $pathology->getLocalisation(),
        ];
    }

    private function frmAdvice($em,$route,$advice) {
        $frmcatory=$this->frmCategory($em,$advice->getCategory());

        $date=null;
        $routeadvice=$em->getRepository("App\Entity\Routeadvice")->findOneBy(["route"=>$route,"advice"=>$advice]);
        if($routeadvice) $date=($routeadvice->getDate()?$routeadvice->getDate()->format("Y-m-d")."T00:00:00Z":null);

        return [
            "id" => $advice->getId(),
            "title" => $advice->getTitle(),
            "description" => $advice->getDescription(),
            "isdate" => $advice->isDate(),
            "date" => $date,
            "category" => $frmcatory,
        ];
    }

    private function frmStep($em,$route,$step) {
        $advices=$step->getAdvices();

        $frmadvices=[];
        foreach($advices as $advice) {
            $pathologys=$advice->getPathologys();
            $tosend=true;
            if(!$pathologys->isEmpty()) {
                $tosend=false;
                foreach($pathologys as $pathology) {
                    if($pathology==$route->getPathology()) $tosend=true;
                }
            }

            if($tosend) array_push($frmadvices,$this->frmAdvice($em,$route,$advice));
        }

        $date=null;
        $routestep=$em->getRepository("App\Entity\Routestep")->findOneBy(["route"=>$route,"step"=>$step]);
        if($routestep) $date=($routestep->getDate()?$routestep->getDate()->format("Y-m-d")."T00:00:00Z":null);

        return [
            "id" => $step->getId(),
            "title" => $step->getTitle(),
            "description" => $step->getDescription(),
            "isdate" => $step->isDate(),
            "chapter" => $step->getChapter(),
            "icon" => $step->getIcon(),
            "date" => $date,
            "tags" => ($step->getTags()?$step->getTags():""),
            "advices" => $frmadvices,
        ];
    }

    private function frmCategory($em,$category) {
        return [
            "id" => $category->getId(),
            "title" => $category->getTitle(),
            "description" => $category->getDescription(),
            "color" => $category->getColor(),
            "icon" => $category->getIcon(),
        ];
    }

    private function iskey($key) {
        return ($key==$this->getParameter("appSecret"));
    }
}
