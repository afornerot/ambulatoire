<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Pathology;
use App\Form\PathologyType;

class PathologyController extends AbstractController
{
    #[Route('/pathology', name: 'app_pathology')]
    public function list(ManagerRegistry $em): Response
    {
        $pathologys = $em->getRepository("App\Entity\Pathology")->findAll();

        return $this->render('pathology/list.html.twig', [
            'usesidebar' => true,
            'pathologys' => $pathologys,
        ]);
    }

    #[Route('/pathology/submit', name: 'app_pathology_submit')]
    public function submit(ManagerRegistry $em,Request $request): Response
    {
        $pathology = new Pathology();
        $form = $this->createForm(PathologyType::class, $pathology, ['mode' => 'submit']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $data = $form->getData();
            $em->getManager()->persist($data);
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_pathology'));
        }

        return $this->render('pathology/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "submit",
            'form' => $form->createView(),
            'pathology' => $pathology,
        ]);
    }  
    
    #[Route('/pathology/update/{id}', name: 'app_pathology_update')]
    public function update($id,ManagerRegistry $em,Request $request): Response
    {
        $pathology = $em->getRepository("App\Entity\Pathology")->find($id);
        $form = $this->createForm(PathologyType::class, $pathology, ['mode' => 'update']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_pathology'));
        }

        return $this->render('pathology/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "update",
            'form' => $form->createView(),
            'pathology' => $pathology,
        ]);
    } 
    
    #[Route('/pathology/delete/{id}', name: 'app_pathology_delete')]
    public function delete($id,ManagerRegistry $em,Request $request): Response
    {
        $pathology = $em->getRepository("App\Entity\Pathology")->find($id);
        
        try {
            $em->getManager()->remove($pathology);
            $em->getManager()->flush();
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            return $this->redirectToRoute('app_pathology_update', ['id' => $id]);
        }

        return $this->redirectToRoute('app_pathology');
    }     

    #[Route('/pathology/select', name: 'app_pathology_select')]
    public function select(ManagerRegistry $em,Request $request): Response
    {

        $output=array();
        $page_limit=$request->query->get('page_limit');
        $q=$request->query->get('q');
        
        $qb = $em->getManager()->createQueryBuilder();
        $qb->select('table')->from('App\Entity\Pathology','table')
           ->where('table.title LIKE :value')
           ->setParameter("value", "%".$q."%")
           ->orderBy('table.title');
        
        $datas=$qb->setFirstResult(0)->setMaxResults($page_limit)->getQuery()->getResult();
        foreach($datas as $data) {
            array_push($output,array("id"=>$data->getId(),"text"=>$data->getTitle()));
        }

        $ret_string["results"]=$output;
        return new JsonResponse($ret_string);    
    } 
}
