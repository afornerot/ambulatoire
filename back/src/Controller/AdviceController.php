<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Advice;
use App\Form\AdviceType;

class AdviceController extends AbstractController
{
    #[Route('/advice', name: 'app_advice')]
    public function list(ManagerRegistry $em): Response
    {
        $advices = $em->getRepository("App\Entity\Advice")->findAll();

        return $this->render('advice/list.html.twig', [
            'usesidebar' => true,
            'advices' => $advices,
        ]);
    }

    #[Route('/advice/submit', name: 'app_advice_submit')]
    public function submit(ManagerRegistry $em,Request $request): Response
    {
        $advice = new Advice();
        $form = $this->createForm(AdviceType::class, $advice, ['mode' => 'submit']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $data = $form->getData();
            $em->getManager()->persist($data);
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_advice'));
        }

        return $this->render('advice/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "submit",
            'form' => $form->createView(),
            'advice' => $advice,
        ]);
    }  
    
    #[Route('/advice/update/{id}', name: 'app_advice_update')]
    public function update($id,ManagerRegistry $em,Request $request): Response
    {
        $advice = $em->getRepository("App\Entity\Advice")->find($id);
        $form = $this->createForm(AdviceType::class, $advice, ['mode' => 'update']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_advice'));
        }

        return $this->render('advice/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "update",
            'form' => $form->createView(),
            'advice' => $advice,
        ]);
    } 
    
    #[Route('/advice/delete/{id}', name: 'app_advice_delete')]
    public function delete($id,ManagerRegistry $em,Request $request): Response
    {
        $advice = $em->getRepository("App\Entity\Advice")->find($id);
        
        try {
            $em->getManager()->remove($advice);
            $em->getManager()->flush();
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            return $this->redirectToRoute('app_advice_update', ['id' => $id]);
        }

        return $this->redirectToRoute('app_advice');
    }     
}
