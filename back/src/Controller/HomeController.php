<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Doctrine\Persistence\ManagerRegistry;

class HomeController extends AbstractController
{
    private $appKernel;
    
    public function __construct(KernelInterface $appKernel)
    {
        $this->appKernel = $appKernel;
    }


    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'usesidebar' => true,
        ]);
    }

    #[Route('/ical/{uuid}', name: 'app_ical')]
    public function ical($uuid,ManagerRegistry $em): Response
    {
        /*
        BEGIN:VCALENDAR
        PRODID:-//Mozilla.org/NONSGML Mozilla Calendar V1.1//EN
        VERSION:2.0
        CALSCALE:GREGORIAN
        X-WR-CALNAME:ambufacile
        X-WR-TIMEZONE:Europe/Paris
        BEGIN:VEVENT
        CREATED:20231014T152301Z
        LAST-MODIFIED:20231014T152449Z
        DTSTAMP:20231014T152449Z
        UID:00e742a4-b5f2-4984-b87d-2611ab652026
        SUMMARY:TestAllday
        DTSTART;VALUE=DATE:20231011
        DTEND;VALUE=DATE:20231012
        END:VEVENT
        END:VCALENDAR
        */

        $route=$em->getRepository("App\Entity\Route")->findOneBy(["uuid" => $uuid]);
        if(!$route) die();

        $dir = $this->appKernel->getProjectDir().'/uploads/ical/';
        $filename = "ical-".$uuid.".ical";
        $fs = new Filesystem();
        $fs->mkdir($dir);
        $file = fopen($dir.$filename, 'w');
        $now = new \DateTime();

        fwrite($file, "BEGIN:VCALENDAR\n");
        fwrite($file, "PRODID:-//Mozilla.org/NONSGML Mozilla Calendar V1.1//EN\n");
        fwrite($file, "VERSION:2.0\n");
        fwrite($file, "CALSCALE:GREGORIAN\n");
        fwrite($file, "X-WR-CALNAME:ambufacile\n");
        fwrite($file, "X-WR-TIMEZONE:Europe/Paris\n");

        $routesteps=$em->getRepository("App\Entity\Routestep")->findBy(["route" => $route]);
        foreach($routesteps as $routestep) {
            if($routestep->getDate()) {
                fwrite($file, "BEGIN:VEVENT\n");
                fwrite($file, "CREATED:".$now->format("Ymd")."T000000Z\n");
                fwrite($file, "LAST-MODIFIED:".$now->format("Ymd")."T000000Z\n");
                fwrite($file, "DTSTAMP:".$now->format("Ymd")."T000000Z\n");
                fwrite($file, "UID:routestep-".$routestep->getId()."\n");
                fwrite($file, "SUMMARY:".$routestep->getStep()->getTitle()."\n");
                fwrite($file, "DTSTART;VALUE=DATE:".$routestep->getDate()->format("Ymd")."\n");
                fwrite($file, "DTEND;VALUE=DATE:".$routestep->getDate()->format("Ymd")."\n");
                fwrite($file, "END:VEVENT\n");
            }            
        }

        $routeadvices=$em->getRepository("App\Entity\Routeadvice")->findBy(["route" => $route]);
        foreach($routeadvices as $routeadvice) {
            if($routeadvice->getDate()) {
                fwrite($file, "BEGIN:VEVENT\n");
                fwrite($file, "CREATED:".$now->format("Ymd")."T000000Z\n");
                fwrite($file, "LAST-MODIFIED:".$now->format("Ymd")."T000000Z\n");
                fwrite($file, "DTSTAMP:".$now->format("Ymd")."T000000Z\n");
                fwrite($file, "UID:routeadvice-".$routeadvice->getId()."\n");
                fwrite($file, "SUMMARY:".$routeadvice->getAdvice()->getTitle()."\n");
                fwrite($file, "DTSTART;VALUE=DATE:".$routeadvice->getDate()->format("Ymd")."\n");
                fwrite($file, "DTEND;VALUE=DATE:".$routeadvice->getDate()->format("Ymd")."\n");
                fwrite($file, "END:VEVENT\n");
            }            
        }

        fwrite($file, "END:VCALENDAR\n");
        fclose($file);

        $response = new BinaryFileResponse($dir.$filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        
        return $response;     
    }

}
