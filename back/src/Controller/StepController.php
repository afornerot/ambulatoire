<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Step;
use App\Form\StepType;

class StepController extends AbstractController
{
    #[Route('/step', name: 'app_step')]
    public function list(ManagerRegistry $em): Response
    {
        $steps01 = $em->getRepository("App\Entity\Step")->findBy(["chapter"=>"PRÉ"],["roworder"=>"ASC"]);
        $steps02 = $em->getRepository("App\Entity\Step")->findBy(["chapter"=>"PER"],["roworder"=>"ASC"]);
        $steps03 = $em->getRepository("App\Entity\Step")->findBy(["chapter"=>"POST"],["roworder"=>"ASC"]);

        return $this->render('step/list.html.twig', [
            'usesidebar' => true,
            'steps01' => $steps01,
            'steps02' => $steps02,
            'steps03' => $steps03,
        ]);
    }

    #[Route('/step/submit', name: 'app_step_submit')]
    public function submit(ManagerRegistry $em,Request $request): Response
    {
        $step = new Step();
        $form = $this->createForm(StepType::class, $step, ['mode' => 'submit']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $data = $form->getData();
            $last=$em->getRepository("App\Entity\Step")->findOneBy(["chapter"=>$data->getChapter()],["roworder"=>"DESC"]);
            $data->setRoworder(($last?$last->getRoworder()+1:0));
            $em->getManager()->persist($data);
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_step'));
        }

        return $this->render('step/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "submit",
            'form' => $form->createView(),
            'step' => $step,
        ]);
    }  
    
    #[Route('/step/update/{id}', name: 'app_step_update')]
    public function update($id,ManagerRegistry $em,Request $request): Response
    {
        $step = $em->getRepository("App\Entity\Step")->find($id);
        $form = $this->createForm(StepType::class, $step, ['mode' => 'update']);
        $form->handleRequest($request);
        if ($form->get('submit')->isClicked() && $form->isValid()) {
            $em->getManager()->flush();
            return $this->redirect($this->generateUrl('app_step'));
        }

        return $this->render('step/edit.html.twig', [
            'usesidebar' => true,
            'mode' => "update",
            'form' => $form->createView(),
            'step' => $step,
        ]);
    } 
    
    #[Route('/step/delete/{id}', name: 'app_step_delete')]
    public function delete($id,ManagerRegistry $em,Request $request): Response
    {
        $step = $em->getRepository("App\Entity\Step")->find($id);
        
        try {
            $em->getManager()->remove($step);
            $em->getManager()->flush();
        } catch (\Exception $e) {
            $request->getSession()->getFlashBag()->add('error', $e->getMessage());
            return $this->redirectToRoute('app_step_update', ['id' => $id]);
        }

        return $this->redirectToRoute('app_step');
    }     

    #[Route('/step/select', name: 'app_step_select')]
    public function select(ManagerRegistry $em,Request $request): Response
    {
        $output=array();
        $page_limit=$request->query->get('page_limit');
        $q=$request->query->get('q');
        
        $qb = $em->getManager()->createQueryBuilder();
        $qb->select('table')->from('App\Entity\Step','table')
           ->where('table.title LIKE :value')
           ->setParameter("value", "%".$q."%")
           ->orderBy('table.title');
        
        $datas=$qb->setFirstResult(0)->setMaxResults($page_limit)->getQuery()->getResult();
        foreach($datas as $data) {
            array_push($output,array("id"=>$data->getId(),"text"=>$data->getTitle()));
        }

        $ret_string["results"]=$output;
        return new JsonResponse($ret_string);    
    } 

    #[Route('/step/roworder', name: 'app_step_roworder')]
    public function roworder(ManagerRegistry $em,Request $request): Response
    {
        $id=$request->get('id');
        $chapter=$request->get('chapter');
        $roworder=$request->get('roworder');
        
        $step = $em->getRepository("App\Entity\Step")->find($id);
        if($step) {
            $step->setChapter($chapter);
            $step->setRoworder($roworder);
            $em->getManager()->flush();
        }

        return new JsonResponse(true);    
    }     
}
