<?php

namespace App\Repository;

use App\Entity\Routeadvice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Routeadvice>
 *
 * @method Routeadvice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Routeadvice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Routeadvice[]    findAll()
 * @method Routeadvice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouteadviceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Routeadvice::class);
    }

//    /**
//     * @return Routeadvice[] Returns an array of Routeadvice objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Routeadvice
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
