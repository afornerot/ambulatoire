SHELL := /bin/bash

up:
	cd back && composer install 
	cd back && docker-compose up -d 
	cd back && symfony server:start -d
	cd back && bin/console app:init
	cd ambulatoire-ui && npm i
	cd ambulatoire-ui && npm run dev

down:

	cd back && docker-compose stop 
	cd back && symfony server:stop


