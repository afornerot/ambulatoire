-- Adminer 4.8.1 PostgreSQL 15.4 dump

\connect "app";

DROP TABLE IF EXISTS "advice";
CREATE TABLE "public"."advice" (
    "id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "description" text,
    "category_id" integer NOT NULL,
    "date" boolean NOT NULL,
    "step_id" integer,
    CONSTRAINT "advice_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_64820e8d12469de2" ON "public"."advice" USING btree ("category_id");

CREATE INDEX "idx_64820e8d73b21e9c" ON "public"."advice" USING btree ("step_id");

INSERT INTO "advice" ("id", "title", "description", "category_id", "date", "step_id") VALUES
(2,	'Prescription anticipée',	'<p>Si vous avez d&eacute;j&agrave; une ordonnance pour des traitements et/ou dispositifs m&eacute;dicaux n&eacute;cessaires apr&egrave;s l&rsquo;intervention pr&eacute;voir de passer &agrave; la pharmacie.</p>',	4,	'f',	5),
(4,	'Activités du quotidien',	'<p>il est possible que l&rsquo;intervention impacte vos gestes du quotidien lors de votre retour &agrave; domicile.</p>',	4,	'f',	5),
(5,	'Prévoir aidant (toilette, garde d’enfants, ménage, repas), ou service social.',	NULL,	6,	't',	5),
(6,	'Soins à domicile :',	'<p>il possible que l&rsquo;intervention n&eacute;cessite des soins &agrave; domicile tels que soins infirmiers et/ou kin&eacute;sith&eacute;rapeute. En fonction de votre lieu de r&eacute;sidence les d&eacute;lais de rendez-vous peuvent varier</p>',	4,	'f',	5),
(7,	'Contacter professionnels libéraux',	NULL,	6,	't',	5),
(8,	'Que puis-je apporter pendant l’intervention ?',	'<p>Lors de votre intervention, si vous choisissez une anesth&eacute;sie locor&eacute;gionale il est peut-&ecirc;tre possible d&rsquo;apporter avec vous des effets personnels tels que&nbsp;: &eacute;couteurs, t&eacute;l&eacute;phone portable, livre&hellip;</p>',	4,	'f',	6),
(9,	'Règles du jeun',	'<p>Il vous est demander de respecter un jeun pr&eacute; op&eacute;ratoire&nbsp;:</p>

<p>H- 6h avant votre arriv&eacute;e &agrave; l&rsquo;h&ocirc;pital pour les solides &nbsp;</p>

<p>H- 2h pour les liquides clairs.</p>

<p>Reportez-vous &agrave; votre passeport ambulatoire</p>',	4,	'f',	6),
(10,	'Débuter le jeun',	NULL,	6,	't',	6),
(11,	'Surveillance post opératoire',	'<p>Demander &agrave; une personne de rester &agrave; vos cot&eacute;s la nuit suivant votre intervention.</p>',	4,	'f',	6),
(12,	'Retour à domicile',	'<p>Pr&eacute;voir votre mode de transport pour votre retour &agrave; domicile, toujours accompagn&eacute;.</p>',	4,	'f',	6),
(13,	'Réserver un VSL',	NULL,	6,	't',	6),
(15,	'Occupations',	'<p>pr&eacute;voir des occupations pour combler les temps d&#39;attente</p>',	4,	'f',	6),
(16,	'Gestion de le douleur',	'<p>Controlez les symptomes douloureux: en respectant les prescriptions m&eacute;dicales, pensez &agrave; anticiper la prise des antalgiques.</p>',	4,	'f',	12),
(3,	'Pharmacie',	NULL,	6,	't',	5);

DROP TABLE IF EXISTS "advice_pathology";
CREATE TABLE "public"."advice_pathology" (
    "advice_id" integer NOT NULL,
    "pathology_id" integer NOT NULL,
    CONSTRAINT "advice_pathology_pkey" PRIMARY KEY ("advice_id", "pathology_id")
) WITH (oids = false);

CREATE INDEX "idx_24550cfc12998205" ON "public"."advice_pathology" USING btree ("advice_id");

CREATE INDEX "idx_24550cfcce86795d" ON "public"."advice_pathology" USING btree ("pathology_id");


DROP TABLE IF EXISTS "category";
CREATE TABLE "public"."category" (
    "id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "description" text,
    "icon" character varying(255) NOT NULL,
    "color" character varying(255) NOT NULL,
    CONSTRAINT "category_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "category" ("id", "title", "description", "icon", "color") VALUES
(4,	'Informations',	NULL,	'FaInfoCircle',	'#f1c232'),
(6,	'Liens agenda',	NULL,	'FaRegCalendarAlt',	'#0b5394'),
(5,	'Penses bête',	NULL,	'FaSplotch',	'#38761d');

DROP TABLE IF EXISTS "messenger_messages";
DROP SEQUENCE IF EXISTS messenger_messages_id_seq;
CREATE SEQUENCE messenger_messages_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."messenger_messages" (
    "id" bigint DEFAULT nextval('messenger_messages_id_seq') NOT NULL,
    "body" text NOT NULL,
    "headers" text NOT NULL,
    "queue_name" character varying(190) NOT NULL,
    "created_at" timestamp(0) NOT NULL,
    "available_at" timestamp(0) NOT NULL,
    "delivered_at" timestamp(0),
    CONSTRAINT "messenger_messages_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_75ea56e016ba31db" ON "public"."messenger_messages" USING btree ("delivered_at");

CREATE INDEX "idx_75ea56e0e3bd61ce" ON "public"."messenger_messages" USING btree ("available_at");

CREATE INDEX "idx_75ea56e0fb7336f0" ON "public"."messenger_messages" USING btree ("queue_name");

COMMENT ON COLUMN "public"."messenger_messages"."created_at" IS '(DC2Type:datetime_immutable)';

COMMENT ON COLUMN "public"."messenger_messages"."available_at" IS '(DC2Type:datetime_immutable)';

COMMENT ON COLUMN "public"."messenger_messages"."delivered_at" IS '(DC2Type:datetime_immutable)';


DELIMITER ;;

CREATE TRIGGER "notify_trigger" AFTER INSERT OR UPDATE ON "public"."messenger_messages" FOR EACH ROW EXECUTE FUNCTION notify_messenger_messages();;

DELIMITER ;

DROP TABLE IF EXISTS "pathology";
CREATE TABLE "public"."pathology" (
    "id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "description" text,
    "localisation" character varying(255) NOT NULL,
    CONSTRAINT "pathology_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "pathology" ("id", "title", "description", "localisation") VALUES
(5,	'Tête',	NULL,	'head'),
(6,	'Jambe Gauche',	NULL,	'leftLeg'),
(7,	'Epaule gauche',	NULL,	'leftShoulder'),
(8,	'Epaule droite',	NULL,	'rightShoulder'),
(9,	'Bras gauche',	NULL,	'leftArm'),
(10,	'Bras droit',	NULL,	'rightArm'),
(11,	'Poitrine',	NULL,	'chest'),
(12,	'Abdomen',	NULL,	'stomach'),
(13,	'Jambe gauche',	NULL,	'leftLeg'),
(14,	'Jambe droite',	NULL,	'rightLeg'),
(16,	'Main droite',	NULL,	'rightHand'),
(15,	'Main gauche',	NULL,	'leftHand'),
(17,	'Pied gauche',	NULL,	'leftFoot'),
(18,	'Pied droit',	NULL,	'rightFoot');

DROP TABLE IF EXISTS "route";
CREATE TABLE "public"."route" (
    "id" integer NOT NULL,
    "uuid" uuid NOT NULL,
    "pathology_id" integer NOT NULL,
    CONSTRAINT "route_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_2c42079ce86795d" ON "public"."route" USING btree ("pathology_id");

INSERT INTO "route" ("id", "uuid", "pathology_id") VALUES
(11,	'555ca3c3-eb0e-4fa2-9295-1f400a34e1ab',	5),
(12,	'f389f770-d79c-4b67-bc8d-895f8ebf9434',	5),
(13,	'f64c7efc-f864-4f83-a8a2-c03e3f75479c',	5),
(14,	'b85a6374-32bd-43fb-8419-eddf8271c4fd',	5),
(15,	'a5554026-adfb-41ff-bd18-2b999c46d65d',	5),
(16,	'f2551d43-a488-47db-99a9-7ec975f82bf5',	5),
(17,	'b177847e-0210-419d-9f42-331de3e08860',	5),
(18,	'9b265009-6504-456c-ac22-c8419095c5bc',	5),
(19,	'8363cd9f-acc1-4a84-abff-5ab680787414',	5),
(20,	'09dd0555-11e1-4b75-9b46-fd98eb44de72',	5),
(21,	'79f64763-5c8f-4596-acf6-ade1f71d888d',	5),
(22,	'b10fa632-60cc-45ee-946d-b9d647fce6b4',	5),
(23,	'cb9f42ad-99d8-404f-a8bb-089b99c911a6',	8),
(24,	'3bb5ccbc-3c6a-4b84-bf05-6b86fa085ad2',	12);

DROP TABLE IF EXISTS "routeadvice";
CREATE TABLE "public"."routeadvice" (
    "id" integer NOT NULL,
    "route_id" integer NOT NULL,
    "advice_id" integer NOT NULL,
    "date" timestamp(0),
    CONSTRAINT "routeadvice_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_73c5d0bf12998205" ON "public"."routeadvice" USING btree ("advice_id");

CREATE INDEX "idx_73c5d0bf34ecb4e6" ON "public"."routeadvice" USING btree ("route_id");

INSERT INTO "routeadvice" ("id", "route_id", "advice_id", "date") VALUES
(2,	14,	8,	'2023-10-16 20:31:33');

DROP TABLE IF EXISTS "routestep";
CREATE TABLE "public"."routestep" (
    "id" integer NOT NULL,
    "route_id" integer NOT NULL,
    "step_id" integer NOT NULL,
    "date" timestamp(0),
    CONSTRAINT "routestep_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "idx_2c5b24ac34ecb4e6" ON "public"."routestep" USING btree ("route_id");

CREATE INDEX "idx_2c5b24ac73b21e9c" ON "public"."routestep" USING btree ("step_id");

INSERT INTO "routestep" ("id", "route_id", "step_id", "date") VALUES
(2,	14,	5,	'2023-12-31 20:32:37');

DROP TABLE IF EXISTS "step";
CREATE TABLE "public"."step" (
    "id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "description" text,
    "roworder" integer NOT NULL,
    "date" boolean NOT NULL,
    "icon" character varying(255) NOT NULL,
    "chapter" character varying(255) NOT NULL,
    "tags" character varying(255),
    CONSTRAINT "step_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "step" ("id", "title", "description", "roworder", "date", "icon", "chapter", "tags") VALUES
(5,	'Rendez vous chirurgien',	NULL,	1,	'f',	'FaRegCalendarAlt',	'PRÉ',	NULL),
(4,	'Prise de rendez vous secrétaire',	NULL,	0,	'f',	'FaPhoneAlt',	'PRÉ',	NULL),
(6,	'Rendez vous anesthésiste',	NULL,	2,	't',	'FaRegCalendarAlt',	'PRÉ',	'Douche'),
(12,	'24h post opératoire',	NULL,	8,	'f',	'FaHouseDamage',	'POST',	NULL),
(13,	'48h post opératoire',	NULL,	9,	'f',	'FaHouseDamage',	'POST',	NULL),
(7,	'Arrivée en service d''ambulatoire',	NULL,	3,	'f',	'FaDoorOpen',	'PER',	NULL),
(8,	'Intervention chirurgicale',	NULL,	4,	't',	'FaUserMd',	'PER',	NULL),
(9,	'Salle de réveil',	NULL,	5,	'f',	'FaBed',	'PER',	NULL),
(10,	'Retour en service d''ambulatoire',	NULL,	6,	'f',	'FaChild',	'PER',	NULL),
(11,	'Retour domicile',	NULL,	7,	'f',	'FaHouseUser',	'POST',	NULL);

DROP TABLE IF EXISTS "user";
CREATE TABLE "public"."user" (
    "id" integer NOT NULL,
    "username" character varying(180) NOT NULL,
    "roles" json NOT NULL,
    "password" character varying(255) NOT NULL,
    CONSTRAINT "uniq_8d93d649f85e0677" UNIQUE ("username"),
    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "user" ("id", "username", "roles", "password") VALUES
(3,	'admin',	'["ROLE_ADMIN"]',	'$2y$13$9cYdYAGEPwnd6q2su3Fe.u9d31xLLZ5nPhP96mL1DMQSBKoaVQoZ6');

ALTER TABLE ONLY "public"."advice" ADD CONSTRAINT "fk_64820e8d12469de2" FOREIGN KEY (category_id) REFERENCES category(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."advice" ADD CONSTRAINT "fk_64820e8d73b21e9c" FOREIGN KEY (step_id) REFERENCES step(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."advice_pathology" ADD CONSTRAINT "fk_24550cfc12998205" FOREIGN KEY (advice_id) REFERENCES advice(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."advice_pathology" ADD CONSTRAINT "fk_24550cfcce86795d" FOREIGN KEY (pathology_id) REFERENCES pathology(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."route" ADD CONSTRAINT "fk_2c42079ce86795d" FOREIGN KEY (pathology_id) REFERENCES pathology(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."routeadvice" ADD CONSTRAINT "fk_73c5d0bf12998205" FOREIGN KEY (advice_id) REFERENCES advice(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."routeadvice" ADD CONSTRAINT "fk_73c5d0bf34ecb4e6" FOREIGN KEY (route_id) REFERENCES route(id) NOT DEFERRABLE;

ALTER TABLE ONLY "public"."routestep" ADD CONSTRAINT "fk_2c5b24ac34ecb4e6" FOREIGN KEY (route_id) REFERENCES route(id) NOT DEFERRABLE;
ALTER TABLE ONLY "public"."routestep" ADD CONSTRAINT "fk_2c5b24ac73b21e9c" FOREIGN KEY (step_id) REFERENCES step(id) NOT DEFERRABLE;

-- 2023-10-15 08:14:58.398206+00
