
::: title
![logo](./tools/logo.png)
:::

::: title  
Défi n°7  
L'Ambulatoire Facile
:::

# L'Ambulatoire Facile

Pouvons-nous proposer au patient pris en charge en ambulatoire, des conseils et une organisation personnalisée, pour un patient serein en pré, per et post opératoire ?

## Présentation du Projet

Le parcours ambulatoire
C'est le fait de prendre en charge un patient à l'hopital dans la journée sans nuité.

Cela permet
- financièrement un turn-over sans frais impactant
- un bienfait pour le patient de retourner chez lui

Par contre
- cela induit un stress pour le patient
- par manque d'information sur sa prise en charge

L'idée serait
- de créer un outils numérique
    - mais avec une alternative possible papier 
    - sauf que la cible principale de la solution reste une population qui est habituée de ce type d'outils
    - un patient agé est la plupard du temps n'est pas dans le circuit en ambulatoire
    - la seule condition est que la solution ne doit pas être obligatoire dans son parcours
- de donner des conseils pré / per / post 
- de permettre au patient d'anticiper 

Attention
- aucune information médicale ne doit être dans la solution
- sur le post opératoire en fonction des sympthomes donnés par le patient lui donner des conseils

## Qui initialise le processus ?

Si c'est le patient
- conseil non personnalisé
- nécéssité de maintenir les conseils

Si c'est le chirurgien
- conseil personnalisable
- implique un rôle fort du médecin

En conclusion c'est le patient qui est libre d'accéder à l'application
Le chirurgien pourra inciter le patient à venir utiliser l'application

## Ce qu'attend le patient

Savoir ce qu'il se passe après son opération
Voilà les points les plus souvent remontés :
- temps de récupération
- attente du médecin pour lui donner un "bon de sorti"
- avoir son ordonnance de post opération
- normalement il devrait avoir son ordonnance dès la prise de rendez-vous
- mais dans les faits il a son ordonnance au moment de sa sortie
- du coup il n'a pas forcement le temps d'aller chercher ses médicaments


## Processus

![logo](./processus.drawio.svg)




