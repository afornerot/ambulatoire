# L'Ambulatoire Facile

Pouvons-nous proposer au patient pris en charge en ambulatoire, des conseils et une organisation personnalisée, pour un patient serein en pré, per et post opératoire ?

Voici le POC réalisé durant le Hacking Health 2023 de Besançon

## Installation

### Dépendance 

- docker-compose
- symfony
- npm


### Lancer le POC

```
make up
```
### Initialiser la BDD

Via adminer http://localhost:6081
Système = PostgresSQL
Serveur = database
Login = app
Password = !ChangeMe!

Importer la bdd via le fichier /tools/database.sql

### Stopper le POC

```
make up
```
